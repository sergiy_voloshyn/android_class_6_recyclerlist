package com.sourceit.firstandroidproject.recyclerlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sourceit.firstandroidproject.R;
import com.sourceit.firstandroidproject.list.Article;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Student on 20.01.2018.
 */

public class ArticleRecyclerAdapter extends RecyclerView.Adapter<ArticleRecyclerAdapter.ViewHolder> {
    private List<Article> list;
    private Context context;
    private OnArticleClick onArticleClick;

    public void setOnArticleClick(OnArticleClick onArticleClick) {
        this.onArticleClick = onArticleClick;
    }

    public  void addToPosition(Article article,int position){

        list.add(position,article);
        notifyItemInserted(position);
    }

    public ArticleRecyclerAdapter(List<Article> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.updateData(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    interface OnArticleClick {

        void onItemClick(int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.text)
        TextView text;
        Article article;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void updateData(Article article) {
            this.article = article;
            title.setText(article.getTitle());
            text.setText(article.getText());

        }

        @OnClick(R.id.root)
        void onItemClick() {

            if (onArticleClick != null) {
                onArticleClick.onItemClick(getAdapterPosition());
            }


        }

    }

}
