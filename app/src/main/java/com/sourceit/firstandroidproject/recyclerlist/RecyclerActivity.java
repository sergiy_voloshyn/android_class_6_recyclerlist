package com.sourceit.firstandroidproject.recyclerlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.sourceit.firstandroidproject.R;
import com.sourceit.firstandroidproject.list.Article;
import com.sourceit.firstandroidproject.list.Generator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerActivity extends AppCompatActivity {


    @BindView (R.id.list) RecyclerView list;
    ArticleRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);
        ButterKnife.bind(this);

        adapter = new ArticleRecyclerAdapter(Generator.generateList(),this);
        LinearLayoutManager layoutManager= new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);
        adapter.setOnArticleClick(new ArticleRecyclerAdapter.OnArticleClick(){
            @Override
            public void onItemClick(int position) {
               // Toast.makeText(RecyclerActivity.this,article.getTitle(),Toast.LENGTH_SHORT).show();

                Article article =new Article("title"+position,"text");
                adapter.addToPosition(article,position);
                list.smoothScrollToPosition(position);

            }
        });





    }
}
