package com.sourceit.firstandroidproject.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.sourceit.firstandroidproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;

public class LeftFragment extends Fragment {

    @BindView(R.id.some_text)
    TextView label;
    @BindView(R.id.input_text_name)
    EditText inputTextName;
    @BindView(R.id.input_text_age)
    EditText inputTextAge;
    Unbinder unbinder;

    public static LeftFragment newInstance() {
        return new LeftFragment();
    }

    ActivityCommunication activityCommunication;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_blank, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActivityCommunication) {
            activityCommunication = (ActivityCommunication) context;
        } else {
            throw new RuntimeException(Context.class.getSimpleName() + " must implement ActivityCommunication interface");
        }
    }

    @OnClick(R.id.action)
    public void onPresentClick() {
        activityCommunication.updateText(label.getText().toString());
    }

    @OnTextChanged({R.id.input_text_age, R.id.input_text_name})
    public void onTextChange(Editable s) {
        label.setText(inputTextName.getText().toString() + ", " +
                inputTextAge.getText().toString());
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
